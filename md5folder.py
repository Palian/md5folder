import hashlib
import os
import datetime
import tika
from tika import parser
import json


dm = 1
ext = ['.doc', '.docx', '.xls', '.xlsx']


def log(d, msg):
    if d:
        print("LOG: {}".format(msg))


def check_ext(fn):
    for e in ext:
        if fn.endswith(e):
            return True
    return False


def crc_folder(src_dir):
    log(dm, src_dir)
    ft = os.walk(src_dir)
    for tops, dirs, files in ft:
        for file in files:
            s = "{}\{}".format(tops, file)
            if check_ext(s):
                with open(s, mode='rb') as fd:
                    filedata = fd.read()

                    filecontent = parser.from_buffer(filedata)
                    if  filecontent["content"] is None:
                        filecontent["content"] = ""
                    #m = hashlib.sha1()     # 1:15
                    #m = hashlib.md5()      # 1:11
                    #m = hashlib.blake2b()  # 0:46
                    #m = hashlib.sha256()   # 1:33
                    #m = hashlib.sha3_256() # 1:23
                    crc_file = hashlib.md5()
                    crc_content = hashlib.md5()
                    crc_file.update(filedata)
                    crc_content.update(filecontent["content"].encode("utf-8"))
                    hex_data = crc_file.hexdigest()
                    hex_content = crc_content.hexdigest()

                    meta_file = {
                        "file": s,
                        "hex_data": hex_data,
                        "hex_content": hex_content
                    }

                    meta_file_json = json.dumps(meta_file, ensure_ascii=False)

                    #print(hex_data, hex_content, s)
                    print(meta_file_json)


if __name__ == '__main__':
    f = "c:\\users\\andrey_2"
    dt_start = datetime.datetime.now()
    crc_folder(f)
    dt_end = datetime.datetime.now()
    log(dm,dt_start)
    log(dm,dt_end)
    log(dm,dt_end - dt_start)